<?php

/*
|--------------------------------------------------------------------------
| Application Constants
|--------------------------------------------------------------------------
|
| 
|
*/

if(!defined('SAMPLE_ID')) define('SAMPLE_ID', 1);

if(!defined('TAKE_COUNT')) define('TAKE_COUNT', 6);

if(!defined('NO')) define('NO', 0);
if(!defined('YES')) define('YES', 1);

if(!defined('PAID')) define('PAID',1);
if(!defined('UNPAID')) define('UNPAID', 0);
if (!defined('PAID_STATUS')) define('PAID_STATUS', 1);


if(!defined('AVAILABLE')) define('AVAILABLE', 1);
if(!defined('NOTAVAILABLE')) define('NOTAVAILABLE', 0);

if(!defined('DATE_AVAILABLE')) define('DATE_AVAILABLE', 1);
if(!defined('DATE_NOTAVAILABLE')) define('DATE_NOTAVAILABLE', 0);


if(!defined('DEVICE_ANDROID')) define('DEVICE_ANDROID', 'android');
if(!defined('DEVICE_IOS')) define('DEVICE_IOS', 'ios');
if(!defined('DEVICE_WEB')) define('DEVICE_WEB', 'web');

if(!defined('APPROVED')) define('APPROVED', 1);
if(!defined('DECLINED')) define('DECLINED', 0);

if(!defined('DEFAULT_TRUE')) define('DEFAULT_TRUE', true);
if(!defined('DEFAULT_FALSE')) define('DEFAULT_FALSE', false);

if(!defined('ADMIN')) define('ADMIN', 'admin');
if(!defined('USER')) define('USER', 'user');
if(!defined('PROVIDER')) define('PROVIDER', 'provider');


if(!defined('COD')) define('COD',   'COD');
if(!defined('PAYPAL')) define('PAYPAL', 'PAYPAL');
if(!defined('CARD')) define('CARD',  'CARD');

if(!defined('STRIPE_MODE_LIVE')) define('STRIPE_MODE_LIVE',  'live');
if(!defined('STRIPE_MODE_SANDBOX')) define('STRIPE_MODE_SANDBOX',  'sandbox');

//////// USERS

if(!defined('USER_TYPE_NORMAL')) define('USER_TYPE_NORMAL', 0);
if(!defined('USER_TYPE_PAID')) define('USER_TYPE_PAID', 1);

if(!defined('USER_PENDING')) define('USER_PENDING', 0);
if(!defined('USER_APPROVED')) define('USER_APPROVED', 1);
if(!defined('USER_DECLINED')) define('USER_DECLINED', 2);

if(!defined('USER_EMAIL_NOT_VERIFIED')) define('USER_EMAIL_NOT_VERIFIED', 0);
if(!defined('USER_EMAIL_VERIFIED')) define('USER_EMAIL_VERIFIED', 1);


//////// USERS END

/***** ADMIN CONTROLS KEYS ********/

if(!defined('ADMIN_CONTROL_ENABLED')) define("ADMIN_CONTROL_ENABLED", 1);

if(!defined('ADMIN_CONTROL_DISABLED')) define("ADMIN_CONTROL_DISABLED", 0);

if(!defined('NO_DEVICE_TOKEN')) define("NO_DEVICE_TOKEN", "NO_DEVICE_TOKEN");
// Subscribed user status

if(!defined('SUBSCRIBED_USER')) define('SUBSCRIBED_USER', 1);

if(!defined('NON_SUBSCRIBED_USER')) define('NON_SUBSCRIBED_USER', 0);

if(!defined('DELETE_STATUS')) define('DELETE_STATUS', -1);


if(!defined('MEETING_INITIATED')) define('MEETING_INITIATED', 0);

if(!defined('MEETING_STARTED')) define('MEETING_STARTED', 1);

if(!defined('MEETING_COMPLETED')) define('MEETING_COMPLETED', 2);


if(!defined('MEETING_MEMBER_JOINED')) define('MEETING_MEMBER_JOINED', 1);

if(!defined('MEETING_MEMBER_ENDED')) define('MEETING_MEMBER_ENDED', 2);


if(!defined('PAYMENT_BY_ADMIN')) define('PAYMENT_BY_ADMIN', 'By Admin');

if(!defined('HRS_TYPE_PER_DAY')) define('HRS_TYPE_PER_DAY', 'per day');
if(!defined('HRS_TYPE_PER_WEEK')) define('HRS_TYPE_PER_WEEK', 'per week'); // not used. Future purpose
if(!defined('HRS_TYPE_PER_MONTH')) define('HRS_TYPE_PER_MONTH', 'per month'); // not used. Future purpose
if(!defined('HRS_TYPE_PER_YEAR')) define('HRS_TYPE_PER_YEAR', 'per year'); // not used. Future purpose


if(!defined('PLAN_TYPE_MONTH_DAY')) define('PLAN_TYPE_MONTH_DAY', 'daily');
if(!defined('PLAN_TYPE_MONTH_WEEK')) define('PLAN_TYPE_MONTH_WEEK', 'weekly'); // not used. Future purpose
if(!defined('PLAN_TYPE_MONTH')) define('PLAN_TYPE_MONTH', 'monthly'); // not used. Future purpose
if(!defined('PLAN_TYPE_MONTH_YEAR')) define('PLAN_TYPE_MONTH_YEAR', 'yearly'); // not used. Future purpose


