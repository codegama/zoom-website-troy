<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Helper, App\Helpers\EnvEditorHelper;

use DB, Hash, Setting, Auth, Validator, Exception, Enveditor;

use App\Admin;

use App\Settings, App\StaticPage;

use App\User;

use App\Subscription, App\UserSubscription;

use App\Meeting, App\MeetingMember;

use App\Jobs\SendEmailJob;

use Carbon\Carbon;


class AdminController extends Controller {
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        $this->middleware('auth:admin');
    }

    /**
     * @method users_index()
     *
     * @uses Show the application dashboard.
     *
     * @created vithya
     *
     * @updated vithya
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function index() {
        
        $data = new \stdClass();
        
        $data->total_users = User::count();

        $data->recent_users = User::orderBy('created_at','DESC')->skip(0)->take(7)->get();

        $data->total_meetings = Meeting::count();

        $data->recent_meetings = Meeting::orderBy('created_at','DESC')->skip(0)->take(7)->get();

        $data->subscription_amount = UserSubscription::where('status',PAID_STATUS)->sum('amount');
              
        return view('admin.dashboard')
                ->with('main_page','dashboard')
                ->with('data',$data);
    }

    /**
     * @method users_index()
     *
     * @uses To list out users details 
     *
     * @created Anjana
     *
     * @updated vithya
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function users_index(Request $request) {

        $users = User::orderBy('updated_at','desc')->paginate(10);

        foreach($users as $user_details){

            $user_added_meeting = Meeting::where('user_id',$user_details->id)->count();

            if($user_added_meeting > 0 ){

                $user_details->is_cleared = Meeting::where('user_id', $user_details->id)->where('status', '!=', MEETING_COMPLETED)->count() ? tr('clear') : tr('cleared');
            
            }else{

                $user_details->is_cleared = tr('meeting_not_found');
            }

        }



        return view('admin.users.index')
                    ->with('main_page', 'users_crud')
                    ->with('page', 'users')
                    ->with('sub_page', 'users-index')
                    ->with('users', $users);
    }

    /**
     * @method users_create()
     *
     * @uses To create a user details
     *
     * @created  Anjana
     *
     * @updated vithya
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function users_create() {

        $user_details = new User;

        return view('admin.users.create')
                    ->with('main_page','users_crud')
                    ->with('page', 'users')
                    ->with('sub_page' , 'users-create')
                    ->with('user_details', $user_details);           
    }

    /**
     * @method users_edit()
     *
     * @uses To display and update user details based on the user id
     *
     * @created Anjana
     *
     * @updated vithya
     *
     * @param integer $user_id
     * 
     * @return redirect view page 
     *
     */
    public function users_edit(Request $request) {

        try {

            $user_details = User::find($request->user_id);

            if(!$user_details) { 

                throw new Exception(tr('user_not_found'), 101);
            }

            return view('admin.users.edit')
                    ->with('main_page','users_crud')
                    ->with('page' , 'users')
                    ->with('sub_page','users-index')
                    ->with('user_details' , $user_details);
            
        } catch(Exception $e) {

            return redirect()->route('admin.users.index')->with('flash_error',  $e->getMessage());
        }
    
    }


    /**
     * @method users_save()
     *
     * @uses To save the users details of new/existing user object based on details
     *
     * @created Anjana
     *
     * @updated vithya
     *
     * @param object user Form Data
     *
     * @return success message
     *
     */
    public function users_save(Request $request) {
        
        try {

            DB::begintransaction();

            $validator = Validator::make( $request->all(), [
                'name' => 'required|max:191',
                'email' => $request->user_id ? 'required|email|max:191|unique:users,email,'.$request->user_id.',id' : 'required|email|max:191|unique:users,email,NULL,id',
                'password' => $request->user_id ? "" : 'required|min:6|confirmed',
                'mobile' => 'required|digits_between:6,13',
                'picture' => 'mimes:jpg,png,jpeg',
                'description' => 'max:191'
                ]
            );

            if($validator->fails()) {

                $error = implode(',', $validator->messages()->all());

                throw new Exception($error, 101);

            }

            $user_details = $request->user_id ? User::find($request->user_id) : new User;

            $is_new_user = NO;

            if($user_details->id) {

                $message = tr('user_updated_success'); 

            } else {

                $is_new_user = YES;

                $user_details->password = ($request->password) ? \Hash::make($request->password) : null;

                $message = tr('user_created_success');

                $user_details->email_verified_at = date('Y-m-d H:i:s');

                $user_details->picture = asset('placeholder.jpg');

                $user_details->is_verified = USER_EMAIL_VERIFIED;

            }

            $user_details->name = $request->name ?: $user_details->name;
            
            $user_details->email = $request->email ?: $user_details->email;

            $user_details->mobile = $request->mobile ?: '';

            $user_details->gender = $request->gender ?? 'male';

            $user_details->description = $request->description ?: '';

            $user_details->login_by = $request->login_by ?: 'manual';

            // Upload picture

            if($request->hasFile('picture') ) {

                if($request->user_id) {

                    Helper::delete_file($user_details->picture, COMMON_FILE_PATH); 
                    // Delete the old pic
                }

                $user_details->picture = Helper::upload_file($request->file('picture'), COMMON_FILE_PATH);
            }

            if($request->user_id) {

               $user_details->updated_at = Carbon::now();
            }

            if($user_details->save()) {

                if($is_new_user == YES) {

                    $email_data['subject'] = tr('user_welcome_title').' '.Setting::get('site_name');

                    $email_data['page'] = "emails.users.welcome";

                    $email_data['data'] = $user_details;

                    $email_data['email'] = $user_details->email;

                    $email_data['password'] = $request->password;

                    $this->dispatch(new SendEmailJob($email_data));

                }
                    
                DB::commit(); 

                return redirect(route('admin.users.view', ['user_id' => $user_details->id]))->with('flash_success', $message);
            } 

            throw new Exception(tr('user_save_failed'));
            
        } catch(Exception $e){ 

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());
        }    
     }

    /**
     * @method users_view()
     *
     * @uses view the users details based on users id
     *
     * @created Anjana 
     *
     * @updated vithya
     *
     * @param integer $user_id
     * 
     * @return View page
     *
     */
    public function users_view(Request $request) {
        
        try {

            $user_details = User::where('id', $request->user_id)->first();


            if(!$user_details) { 

                throw new Exception(tr('user_not_found'), 101);                
            }            
                 
            return view('admin.users.view')
                        ->with('main_page','users_crud')
                        ->with('page', 'users') 
                        ->with('sub_page','users-index') 
                        ->with('user_details' , $user_details);

            
        } catch (Exception $e) {

            $error = $e->getMessage();

            return redirect()->back()->withInput()->with('flash_error', $error);
        }
    
    }


    /**
     * @method users_delete()
     *
     * @uses delete the user details based on user id
     *
     * @created Anjana
     *
     * @updated  
     *
     * @param integer $request id
     * 
     * @return response of success/failure details with view page
     *
     */
    public function users_delete(Request $request) {
            
        try {

            DB::begintransaction();

            $user_details = User::where('id',$request->user_id)->first();
            
            if(!$user_details) {

                throw new Exception(tr('user_not_found'), 101);                
            }

            if($user_details->delete()) {

                DB::commit();

                return redirect()->route('admin.users.index')->with('flash_success',tr('user_deleted_success'));   

            } 
            
            throw new Exception(tr('user_delete_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            $error = $e->getMessage();

            return redirect()->back()->withInput()->with('flash_error', $error);

        }       
         
    }

    /**
     * @method users_status
     *
     * @uses To delete the users details based on users id
     *
     * @created Anjana
     *
     * @updated 
     *
     * @param integer $users_id
     * 
     * @return response success/failure message
     *
     **/
    public function users_status(Request $request) {

        try {

            DB::beginTransaction();

            $user_details = User::find($request->user_id);

            if(!$user_details) {

                throw new Exception(tr('user_not_found'), 101);
                
            }

            $user_details->status = $user_details->status ? DECLINED : APPROVED;

            if($user_details->save()) {

                DB::commit();

                $message = $user_details->status ? tr('user_approve_success') : tr('user_decline_success');

                return redirect()->back()->with('flash_success', $message);
            }
            
            throw new Exception(tr('user_status_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            $error = $e->getMessage();

            return redirect()->route('admin.users.index')->with('flash_error', $error);

        }

    }

    /**
     * @method users_verify_status()
     *
     * @uses verify for the user
     *
     * @created Anjana
     *
     * @updated
     *
     * @param integer $request id
     *
     * @return redirect back page with status of the user verification
     */
    public function users_verify_status(Request $request) {

        try {

            DB::beginTransaction();

            $user_details = User::find($request->user_id);

            if(!$user_details) {

                throw new Exception(tr('user_details_not_found'), 101);
                
            }

            $user_details->is_verified = $user_details->is_verified ? USER_EMAIL_NOT_VERIFIED : USER_EMAIL_VERIFIED;

            if($user_details->save()) {

                DB::commit();

                $message = $user_details->is_verified ? tr('user_verify_success') : tr('user_unverify_success');

                return redirect()->route('admin.users.index')->with('flash_success', $message);
            }
            
            throw new Exception(tr('user_verify_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            $error = $e->getMessage();

            return redirect()->back()->with('flash_error', $error);

        }
    
    }

    /**
     *
     * @method settings()
     *
     * @uses used to view the settings page
     *
     * @created Anjana 
     *
     * @updated 
     *
     * @param - 
     *
     * @return view page
     */

    public function settings() {

        $env_values = EnvEditorHelper::getEnvValues();

        return view('admin.settings.settings')
                ->with('env_values',$env_values)
                ->with('main_page' , 'settings');
   
    }

    /**
     * Function: admin_control()
     * 
     * @uses to admin control options
     *
     * @created Bhawya
     *
     * @updated
     *
     * @param (request) setting details
     *
     * @return success/error message
     */
    public function admin_control() {
           
        return view('admin.settings.control')->with('page', tr('admin_control'));
        
    }

    /**
     * Function: settings_save()
     * 
     * @uses to update settings details
     *
     * @created Anjana H
     *
     * @updated Anjana H
     *
     * @param (request) setting details
     *
     * @return success/error message
     */
    public function settings_save(Request $request) {

        try {
            
            DB::beginTransaction();
            
            $validator = Validator::make($request->all() , 
                [
                    'site_logo' => 'mimes:jpeg,jpg,bmp,png',
                    'site_icon' => 'mimes:jpeg,jpg,bmp,png',

                ],
                [
                    'mimes' => tr('image_error')
                ]
            );

            if($validator->fails()) {

                $error = implode(',', $validator->messages()->all());

                throw new Exception($error, 101);
            }

            foreach( $request->toArray() as $key => $value) {

                if($key != '_token') {

                    $check_settings = Settings::where('key' ,'=', $key)->count();

                    if( $check_settings == 0 ) {

                        throw new Exception( $key.tr('settings_key_not_found'), 101);
                    }
                    
                    if( $request->hasFile($key) ) {
                                            
                        $file = Settings::where('key' ,'=', $key)->first();
                       
                        Helper::delete_file($file->value, FILE_PATH_SITE);

                        $file_path = Helper::upload_file($request->file($key) , FILE_PATH_SITE);    

                        $result = Settings::where('key' ,'=', $key)->update(['value' => $file_path]); 

                        if( $result == TRUE ) {
                     
                            DB::commit();
                   
                        } else {

                            throw new Exception(tr('settings_save_error'), 101);
                        } 
                   
                    } else {
                    
                        $result = Settings::where('key' ,'=', $key)->update(['value' => $value]);  
                    
                        if( $result == TRUE ) {
                         
                            DB::commit();
                       
                        } else {

                            throw new Exception(tr('settings_save_error'), 101);
                        } 

                    }  
 
                }
            }

            Helper::settings_generate_json();

            return back()->with('flash_success', tr('settings_update_success'));
            
        } catch (Exception $e) {

            DB::rollback();

            $error = $e->getMessage();

            return back()->with('flash_error', $error);
        }
    }

    /**
     * @method env_settings_save()
     *
     * @uses used to update the email details for .env file
     *
     * @created Anjana
     *
     * @updated
     *
     * @param Form data
     *
     * @return view page
     */

    public function env_settings_save(Request $request) {

        try {

            $env_settings = ['MAIL_DRIVER','MAIL_HOST', 'MAIL_PORT' , 'MAIL_USERNAME' , 'MAIL_PASSWORD' , 'MAIL_ENCRYPTION' , 'MAILGUN_DOMAIN' , 'MAILGUN_SECRET' , 'FCM_SERVER_KEY', 'FCM_SENDER_ID' , 'FCM_PROTOCOL', 'MAIL_FROM_ADDRESS', 'MAIL_FROM_NAME'];

            if($env_settings){

                foreach ($env_settings as $key => $data) {

                    if($request->$data){ 

                        \Enveditor::set($data,$request->$data);

                    } else{

                        \Enveditor::set($data,$request->$data);
                    }
                }
            }

            $message = tr('settings_update_success');

            return redirect()->route('clear-cache')->with('flash_success', $message);  

        } catch(Exception $e) {

            $error = $e->getMessage();

            return back()->withInput()->with('flash_error' , $error);

        }  

    }

    /**
     * @method profile()
     *
     * @uses  Used to display the logged in admin details
     *
     * @created Anjana
     *
     * @updated
     *
     * @param 
     *
     * @return view page 
     */

    public function profile() {

        $admin_details = Auth::guard('admin')->user();

        return view('admin.account.profile')
                ->with('main_page' , 'profile')
                ->with('admin_details',$admin_details);

    }

    /**
     * @method profile_save()
     *
     * @uses Used to update the admin details
     *
     * @created Anjana
     *
     * @updated
     *
     * @param -
     *
     * @return view page 
     */

    public function profile_save(Request $request) {
       
        try {

            DB::beginTransaction();

            $validator = Validator::make( $request->all(), [
                    'name' => 'max:191',
                    'email' => $request->admin_id ? 'email|max:191|unique:admins,email,'.$request->admin_id : 'email|max:191|unique:admins,email,NULL',
                    'admin_id' => 'required|exists:admins,id',
                    'picture' => 'mimes:jpeg,jpg,png'
                ]
            );
            
            if($validator->fails()) {

                $error = implode(',', $validator->messages()->all());

                throw new Exception($error, 101);
                
            }

            $admin_details = Admin::find($request->admin_id);

            if(!$admin_details) {

                Auth::guard('admin')->logout();

                throw new Exception(tr('admin_details_not_found'), 101);

            }
        
            $admin_details->name = $request->name ?: $admin_details->name;

            $admin_details->email = $request->email ?: $admin_details->email;

            $admin_details->mobile = $request->mobile ?: $admin_details->mobile;

            $admin_details->about = $request->about ?: $admin_details->about;
 
            if($request->hasFile('picture') ) {
                
                Helper::delete_file($admin_details->picture, PROFILE_PATH_ADMIN); 
                
                $admin_details->picture = Helper::upload_file($request->file('picture'), PROFILE_PATH_ADMIN);
            }
            
            $admin_details->remember_token = Helper::generate_token();

            $admin_details->timezone = $request->timezone ?: $admin_details->timezone;

            $admin_details->save();

            DB::commit();

            return redirect()->route('admin.profile')->with('flash_success', tr('admin_profile_success'));


        } catch(Exception $e) {

            DB::rollback();

            $error = $e->getMessage();

            return redirect()->back()->withInput()->with('flash_error' , $error);

        }    
    
    }

    /**
     * @method change_password()
     *
     * @uses  Used to change the admin password
     *
     * @created Anjana
     *
     * @updated
     *
     * @param 
     *
     * @return view page 
     */

    public function change_password(Request $request) {

        try {

            DB::begintransaction();

            $validator = Validator::make($request->all(), [              
                'password' => 'required|min:6',
                'old_password' => 'required',
                'confirm_password' => 'required|min:6'
            ]);
            
            if($validator->fails()) {

                $error = implode(',', $validator->messages()->all());

                throw new Exception($error, 101);
                
            }

            $admin_details = Admin::find(Auth::guard('admin')->user()->id);

            if(!$admin_details) {

                Auth::guard('admin')->logout();

                throw new Exception(tr('admin_details_not_found'), 101);

            }

            if(Hash::check($request->old_password,$admin_details->password)) {

                $admin_details->password = Hash::make($request->password);

                $admin_details->save();

                DB::commit();

                Auth::guard('admin')->logout();

                return redirect()->route('admin.login')->with('flash_success', tr('password_change_success'));
                
            } else {

                throw new Exception(tr('password_mismatch'));
            }


        } catch(Exception $e) {

            DB::rollback();

            $error = $e->getMessage();

            return redirect()->back()->withInput()->with('flash_error' , $error);

        }    
    
    }

    /**
     * @method help()
     *
     * @uses display contact details
     *
     * @created Anjana 
     *
     * @updated
     *
     * @param 
     *
     * @return view page 
     */

    public function help(Request $request) {

        return view('admin.help')
                ->with('page' , 'help')
                ->with('sub_page' , '');

    }

     /**
     * @method static_pages_index()
     *
     * @uses Used to list the static pages
     *
     * @created vithya
     *
     * @updated vithya  
     *
     * @param -
     *
     * @return List of pages   
     */

    public function static_pages_index() {

        $static_pages = StaticPage::orderBy('updated_at' , 'desc')->paginate(10);

        return view('admin.static-pages.index')
                    ->with('main_page','static_pages_crud')
                    ->with('page','static_pages')
                    ->with('sub_page','static_pages_view')
                    ->with('static_pages', $static_pages);
    
    }

    /**
     * @method static_pages_create()
     *
     * @uses To create static_page details
     *
     * @created vithya
     *
     * @updated Anjana   
     *
     * @param
     *
     * @return view page   
     *
     */
    public function static_pages_create() {

        $static_keys = ['about' , 'contact', 'privacy' , 'terms' , 'help' , 'faq' , 'refund', 'cancellation'];

        foreach ($static_keys as $key => $static_key) {

            // Check the record exists

            $check_page = StaticPage::where('type', $static_key)->first();

            if($check_page) {
                unset($static_keys[$key]);
            }
        }

        $static_keys[] = 'others';

        $static_page_details = new StaticPage;

        return view('admin.static-pages.create')
                ->with('main_page','static_pages_crud')
                ->with('page','static_pages')
                ->with('sub_page',"static_pages_create")
                ->with('static_keys', $static_keys)
                ->with('static_page_details',$static_page_details);
   
    }

    /**
     * @method static_pages_edit()
     *
     * @uses To display and update static_page details based on the static_page id
     *
     * @created Anjana
     *
     * @updated vithya
     *
     * @param object $request - static_page Id
     * 
     * @return redirect view page 
     *
     */
    public function static_pages_edit(Request $request) {

        try {

            $static_page_details = StaticPage::find($request->static_page_id);

            if(!$static_page_details) {

                throw new Exception(tr('static_page_not_found'), 101);
            }

            $static_keys = ['about' , 'contact' , 'privacy' , 'terms' , 'help' , 'faq' , 'refund'];

            foreach ($static_keys as $key => $static_key) {

                // Check the record exists

                $check_page = StaticPage::where('type', $static_key)->first();

                if($check_page) {
                    unset($static_keys[$key]);
                }
            }

            $static_keys[] = 'others';

            $static_keys[] = $static_page_details->type;

            return view('admin.static-pages.edit')
                    ->with('main_page','static_pages_crud')
                    ->with('page' , 'static_pages')
                    ->with('sub_page','static_pages_view')
                    ->with('static_keys' , array_unique($static_keys))
                    ->with('static_page_details' , $static_page_details);
            
        } catch(Exception $e) {

            $error = $e->getMessage();

            return redirect()->route('admin.static_pages.index')->with('flash_error' , $error);

        }
    }

    /**
     * @method static_pages_save()
     *
     * @uses Used to create/update the page details 
     *
     * @created vithya
     *
     * @updated vithya
     *
     * @param
     *
     * @return index page    
     *
     */
    public function static_pages_save(Request $request) {

        try {

            DB::beginTransaction();

            $validator = Validator::make($request->all(), [              
                'description' => 'required',
                'type' => !$request->static_page_id ? 'required' : "",
                'title' => $request->static_page_id ? 'required|max:255|unique:static_pages,title,'.$request->static_page_id : 'required|max:255|unique:static_pages,title',

            ]);
            
            if($validator->fails()) {

                $error = implode(',', $validator->messages()->all());

                throw new Exception($error, 101);
                
            }
            if($request->static_page_id != '') {

                $static_page_details = StaticPage::find($request->static_page_id);

                $message = tr('static_page_updated_success');                    

            } else {

                $check_page = "";

                // Check the staic page already exists

                if($request->type != 'others') {

                    $check_page = StaticPage::where('type',$request->type)->first();

                    if($check_page) {

                        return back()->with('flash_error',tr('static_page_already_alert'));
                    }

                }

                $message = tr('static_page_created_success');

                $static_page_details = new StaticPage;

                $static_page_details->status = APPROVED;

            }

            $static_page_details->title = $request->title ?: $static_page_details->title;

            $static_page_details->description = $request->description ?: $static_page_details->description;

            $static_page_details->type = $request->type ?? $static_page_details->type;

            $unique_id = $request->type ?? $static_page_details->type;

            if(!in_array($request->type ?? $static_page_details->type, ['about', 'privacy', 'terms', 'contact', 'help', 'faq'])) {

                $unique_id = routefreestring($request->title ?? rand());

                $unique_id = in_array($unique_id, ['about', 'privacy', 'terms', 'contact', 'help', 'faq']) ? $unique_id.rand() : $unique_id;

            }

            $static_page_details->unique_id = $unique_id ?? rand();

            if($static_page_details->save()) {

                DB::commit();
                
                Helper::settings_generate_json();

                return redirect()->route('admin.static_pages.view', ['static_page_id' => $static_page_details->id] )->with('flash_success', $message);

            } 

            throw new Exception(tr('static_page_save_failed'), 101);
                      
        } catch(Exception $e) {

            DB::rollback();

            return back()->withInput()->with('flash_error', $e->getMessage());

        }
    
    }

    /**
     * @method static_pages_delete()
     *
     * Used to view file of the create the static page 
     *
     * @created vithya
     *
     * @updated vithya R
     *
     * @param -
     *
     * @return view page   
     */

    public function static_pages_delete(Request $request) {

        try {

            DB::beginTransaction();

            $static_page_details = StaticPage::find($request->static_page_id);

            if(!$static_page_details) {

                throw new Exception(tr('static_page_not_found'), 101);
                
            }

            if($static_page_details->delete()) {

                DB::commit();

                return redirect()->route('admin.static_pages.index')->with('flash_success',tr('static_page_deleted_success')); 

            } 

            throw new Exception(tr('static_page_error'));

        } catch(Exception $e) {

            DB::rollback();

            $error = $e->getMessage();

            return redirect()->route('admin.static_pages.index')->with('flash_error', $error);

        }
    
    }

    /**
     * @method static_pages_view()
     *
     * @uses view the static_pages details based on static_pages id
     *
     * @created Anjana 
     *
     * @updated vithya
     *
     * @param object $request - static_page Id
     * 
     * @return View page
     *
     */
    public function static_pages_view(Request $request) {

        $static_page_details = StaticPage::find($request->static_page_id);

        if(!$static_page_details) {
           
            return redirect()->route('admin.static_pages.index')->with('flash_error',tr('static_page_not_found'));
        }

        return view('admin.static-pages.view')
                    ->with('main_page', 'static_pages_crud')
                    ->with('page', 'static_pages')
                    ->with('sub_page', 'static_pages_view')
                    ->with('static_page_details', $static_page_details);
    }

    /**
     * @method static_pages_status_change()
     *
     * @uses To update static_page status as DECLINED/APPROVED based on static_page id
     *
     * @created vithya
     *
     * @updated vithya
     *
     * @param - integer static_page_id
     *
     * @return view page 
     */

    public function static_pages_status_change(Request $request) {

        try {

            DB::beginTransaction();

            $static_page_details = StaticPage::find($request->static_page_id);

            if(!$static_page_details) {

                throw new Exception(tr('static_page_not_found'), 101);
            }

            $static_page_details->status = $static_page_details->status == DECLINED ? APPROVED : DECLINED;

            $static_page_details->save();

            DB::commit();

            $message = $static_page_details->status == DECLINED ? tr('static_page_decline_success') : tr('static_page_approve_success');

            return redirect()->back()->with('flash_success', $message);

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method subscriptions_index()
     * 
     * @uses To list the subscriptions
     *
     * @created Anjana H
     *
     * @updated Anjana H
     *
     * @param
     *
     * @return view page
     */
    public function subscriptions_index() {

        $subscriptions = Subscription::orderBy('created_at','desc')->whereNotIn('status', [DELETE_STATUS])->paginate(10);

        foreach ($subscriptions as $key => $subscription_details) {

            $subscription_details->subscribers_count = UserSubscription::where('subscription_id', $subscription_details->id)->where('status' , 1)->distinct()->count();
             
        }



        return view('admin.subscriptions.index')
            ->with('main_page','subscriptions_crud')
            ->with('page', 'subscriptions')
            ->with('sub_page','subscriptions-view')
            ->with('subscriptions' , $subscriptions);
    }

    /**
     * @method subscriptions_create()
     *
     * @uses To create subscription object 
     *
     * @created Anjana H 
     *
     * @updated Anjana H
     *
     * @param
     *
     * @return View page
     */
    public function subscriptions_create() {

        $subscriptions = new Subscription;

        return view('admin.subscriptions.create')
            ->with('main_page','subscriptions_crud')
            ->with('page', 'subscriptions')
            ->with('sub_page','subscriptions-create')
            ->with('subscription_details' , $subscriptions);

    }

    /**
     * @method subscriptions_edit()
     *
     * @uses To display and update subscription object details based on the subscription id
     *
     * @created  Anjana H
     *
     * @updated Anjana H
     *
     * @param Integer (request) $subscription_id
     *
     * @return View page
     */
    public function subscriptions_edit(Request $request) {

        try {
          
            $subscription_details = Subscription::find($request->subscription_id);

            if(!$subscription_details) {

                throw new Exception(tr('admin_subscription_not_found'), 101);
            }

            return view('admin.subscriptions.edit')
                    ->with('page' , 'subscriptions')
                    ->with('sub_page','subscriptions-view')
                    ->with('subscription_details', $subscription_details);           

        } catch( Exception $e) {

            return redirect()->route('admin.subscriptions.index')->with('flash_error',$e->getMessage());
        }

    }
    
    /**
     * @method subscriptions_save()
     *
     * @uses To save the subscription object details of new/existing based on details
     *
     * @created Anjana H
     *
     * @updated Anjana H
     *
     * @param Integer (request) $subscription_id , (request) subscription details
     *
     * @return success/error message
     */
    public function subscriptions_save(Request $request) {

        try {

            $validator = Validator::make($request->all(),[
                'title' => $request->subscription_id ? 'required|max:255|unique:subscriptions,title,'.$request->subscription_id : 'required|max:255|unique:subscriptions,title',
                'plan' => 'required|numeric|min:1|max:12',
                'amount' => 'required|numeric',
                'no_of_users'=>'required|numeric|min:1',
            ]);
            
            if($validator->fails() ) {

                $error = implode(',', $validator->messages()->all() );

                throw new Exception($error, 101);

            } else {

                if( $request->popular_status == TRUE ) {

                    Subscription::where('popular_status' , TRUE )->update(['popular_status' => FALSE]);
                }

                if($request->subscription_id) {

                    $subscription_details = Subscription::find($request->subscription_id);

                    $subscription_details->update($request->all());

                    $message = tr('admin_subscription_update_success');

                } else {

                    $subscription_details = Subscription::create($request->all());

                    $subscription_details->status = APPROVED ;

                    $subscription_details->popular_status = $request->popular_status == APPROVED ? APPROVED  : DECLINED ;

                    $subscription_details->unique_id = $subscription_details->title;

                    $subscription_details->no_of_users = $request->no_of_users ?? 4;

                    $subscription_details->no_of_hrs = $request->no_of_hrs ?? 1;

                    $message = tr('admin_subscription_create_success'); 

                }

                if($subscription_details->save() ) { 

                    DB::commit();

                    return redirect()->route('admin.subscriptions.view', ['subscription_id' => $subscription_details->id] )->with('flash_success', $message);

                } else { 

                    throw new Exception(tr('admin_subscription_save_error'), 101);
                }
            }
            
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.subscriptions.index')->with('flash_error',$e->getMessage());
        }    
        
    }

    /**
     * @method subscriptions_view()
     * 
     * @uses To display subscription details based on subscription id
     *
     * @created Anjana H
     *
     * @updated Anjana H
     *
     * @param Integer (request) $subscriptions_id
     *
     * @return view page
     */
    public function subscriptions_view(Request $request) {

        try {

            $subscription_details = Subscription::find($request->subscription_id);
            
            if(!$subscription_details) {

                throw new Exception(tr('admin_subscription_not_found'), 101);
                
            }

            $earnings = $subscription_details->userSubscription()->where('status' , APPROVED)->sum('amount');

            $total_subscribers = $subscription_details->userSubscription()->where('status' , APPROVED)->count();


            return view('admin.subscriptions.view')
                        ->with('page' ,'subscriptions')
                        ->with('sub_page' ,'subscriptions-view')
                        ->with('subscription_details' , $subscription_details)
                        ->with('total_subscribers', $total_subscribers)
                        ->with('earnings', $earnings);

        } catch (Exception $e) {

            return redirect()->route('admin.subscriptions.index')->with('flash_error',$e->getMessage());

        }

    }

    /**
     * @method subscriptions_delete()
     * 
     * @uses To delete the subscription object based on subscription id
     *
     * @created Anjana H
     *
     * @updated Anjana H
     *
     * @param 
     *
     * @return success/failure message
     */
    public function subscriptions_delete(Request $request) {

        try {

            DB::beginTransaction();

            $subscription_details = Subscription::find($request->subscription_id);

            $subscription_details->status = DELETE_STATUS;

            if( $subscription_details->save() ) {

                DB::commit();
                
                return redirect()->route('admin.subscriptions.index')->with('flash_success', tr('admin_subscription_delete_success'));

            } else {

                throw new Exception(tr('admin_subscription_delete_error'), 101);
            } 
        
        } catch( Exception $e) {
            
            DB::rollback();
            
            return redirect()->route('admin.subscriptions.index')->with('flash_error',$e->getMessage());
        }
    }


    /**
     * @method subscriptions_popular_status()
     * 
     * @uses To update subscription's popular_status to APPROVED/DECLINED based on subscription id
     *
     * @created Anjana H
     *
     * @updated Anjana H
     *
     * @param 
     *
     * @return success/failure message
     */
    public function subscriptions_popular_status(Request $request) {

        try {

            if($request->has('subscription_id')) {  
            
                $subscription_details = Subscription::where('id', $request->subscription_id)->first();
                
                if(!$subscription_details) {

                    throw new Exception(tr('admin_subscription_not_found'), 101);
                }

                $subscription_details->popular_status  = $subscription_details->popular_status == APPROVED ? DECLINED : APPROVED ;

                $message = $subscription_details->popular_status ? tr('admin_subscription_popular_success') : tr('admin_subscription_remove_popular_success'); 
                
                if( $subscription_details->save() ) { 

                    DB::commit();

                    return back()->with('flash_success' , $message);
                
                } else {

                    throw new Exception(tr('admin_subscription_populor_status_error'), 101);
                }

            }   else {

                throw new Exception( tr('try_again'), 101);
            }  
            
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.subscriptions.index')->with('flash_error',$e->getMessage());
        }
    }

    /**
     * @method subscriptions_users()
     * 
     * @uses To update subscription's popular_status to APPROVED/DECLINED based on subscription id
     *
     * @created Anjana H
     *
     * @updated Anjana H
     *
     * @param 
     *
     * @return success/failure message
     */
    public function subscriptions_users(Request $request) {

        try {

            if($request->has('subscription_id')) {     

                $user_ids = [];

                $user_payments = UserSubscription::where('subscription_id' , $request->subscription_id)->select('user_id')->get();

                foreach ($user_payments as $key => $value) {

                    $user_ids[] = $value->user_id;
                }

                $subscription_details = Subscription::find($request->subscription_id);

                $users = User::whereIn('id' , $user_ids)->orderBy('created_at','desc')->paginate(10);

                foreach($users as $user_details){

                    $user_details->is_cleared = Meeting::where('user_id', $user_details->id)->where('status', '!=', MEETING_COMPLETED)->count() ? tr('clear') : tr('cleared');
        
                }

                return view('admin.users.index')
                            ->withPage('users')
                            ->with('sub_page','users-view')
                            ->with('user_payments' , $user_payments)
                            ->with('users' , $users)
                            ->with('subscription_details' , $subscription_details);
            }   else {

                throw new Exception( tr('try_again'), 101);
            }     
            
        } catch (Exception $e) {         

            return redirect()->route('admin.subscriptions.index')->with('flash_error',$e->getMessage());
        }
    }

    /**
     * @method subscriptions_status_change()
     *
     * @uses To update subscription status to approve/decline based on subscription id
     *
     * @created Anjana H
     *
     * @updated Anjana H
     *
     * @param Integer (request) $subscription_id
     *
     * @return success/error message
     */
    public function subscriptions_status_change(Request $request) {

        try {

            DB::beginTransaction();
       
            $subscription_details = Subscription::find($request->subscription_id);

            if(!$subscription_details) {
                
                throw new Exception(tr('admin_subscription_not_found'), 101);
            } 
            
            $subscription_details->status = $subscription_details->status == APPROVED ? DECLINED : APPROVED;

            $message = $subscription_details->status == APPROVED ? tr('admin_subscription_approved_success') : tr('admin_subscription_declined_success');

            if( $subscription_details->save() ) {

                DB::commit();

                return back()->with('flash_success',$message);

            } else {

                throw new Exception(tr('admin_subscription_status_save_error'), 101);
            }

        } catch( Exception $e) {

            DB::rollback();

            return redirect()->route('admin.subscriptions.index')->with('flash_error',$e->getMessage());
        }

    }  

    /**
     * @method subscription_payments()
     * 
     * @uses to list the subscription_payments
     *
     * @created Anjana H
     *
     * @updated Anjana H
     *
     * @param
     *
     * @return view page
     */
    public function subscription_payments(Request $request) {

        $payments = UserSubscription::orderBy('created_at' , 'desc')
                    ->when($request->subscription_id, function ($query) use ($request) { 
                        return $query->where('subscription_id', $request->subscription_id);
                    })
                   ->paginate(10);

        $subscription_details = $request->has('subscription_id')? Subscription::find($request->subscription_id):'';

        return view('admin.payments.subscription_payments')
            ->with('main_page' , 'subscription_payments')
            ->with('subscription_details' , $subscription_details->title??'')
            ->with('payments' , $payments);

    }

    /**
     * @method meetings()
     * 
     * @uses to list the meetings
     *
     * @created Anjana H
     *
     * @updated Anjana H
     *
     * @param
     *
     * @return view page
     */
    public function meetings_index(Request $request) {

        try {

            if($request->user_id) {

                $user_details = User::find($request->user_id);

                if(!$user_details) { 

                    throw new Exception(tr('user_not_found'), 101);
                }
            }
            
            $meetings = Meeting::orderBy('created_at' , 'desc')
                    ->when($request->meeting_member_id, function ($query) use ($request) { 

                        return $query->leftJoin('meeting_members', 'meeting_members.id', '=', 'meeting_members.meeting_id')

                            ->where(function($query) use($request) {
                                $query->where('meeting_members.user_id', $request->meeting_member_id);
                                });
                    })
                    ->when($request->user_id, function($query) use ($request){
                        return $query->where('meetings.user_id', $request->user_id);   
                    })->paginate(10);


            return view('admin.meetings.index')
                        ->with('main_page', 'meetings')
                        ->with('meetings', $meetings)
                        ->with('page',$request->page??''); 

        } catch (Exception $e) {
                
            return redirect()->back()->with('flash_error', $e->getMessage());
        }

    }


    /**
     * @method meetings()
     * 
     * @uses to view the meetings
     *
     * @created Ganesh
     *
     * @updated Ganesh
     *
     * @param
     *
     * @return view page
     */

    public function meetings_view(Request $request){

        try {

            $meeting_details = Meeting::where('meetings.id', $request->meeting_id)->first();

            $meeting_details->user_details = $meeting_details->user;
           
            $meeting_details->members = MeetingMember::where('meeting_id',$request->meeting_id)->get();
            
            if(!$meeting_details) { 

                throw new Exception(tr('meeting_not_found'), 101);                
            }            
      
            return view('admin.meetings.view')
                        ->with('main_page','meetings')
                        ->with('page', 'meetings') 
                        ->with('sub_page','meetings-view') 
                        ->with('meeting_details' , $meeting_details);

            
        } catch (Exception $e) {

            $error = $e->getMessage();

            return redirect()->back()->withInput()->with('flash_error', $error);
        }

    }


    /**
     * @method meetings_delete()
     *
     * @uses delete the meeting details based on meeting id
     *
     * @created Anjana H
     *
     * @updated Anjana H 
     *
     * @param object $request - meeting Id
     * 
     * @return success/failure details with view page
     *
     */
    public function meetings_delete(Request $request) {

        try {

            DB::begintransaction();

            $meeting_details = Meeting::find($request->meeting_id);
            
            if(!$meeting_details) {

                throw new Exception(tr('meeting_not_found'), 101);
            }

            if($meeting_details->delete()) {

                DB::commit();

                if($request->has('page') && $request->page!=''){
                
                return redirect()->route('admin.meetings.index',['page'=>$request->page])->with('flash_success',tr('meeting_deleted_success'));
               
                }else{

                return redirect()->route('admin.meetings.index')->with('flash_success',tr('meeting_deleted_success'));

                }
                 
            }    
            
            throw new Exception(tr('meeting_delete_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());
        }       
         
    }    

    /**
     * @method meetings_end()
     *
     * @uses end the meeting details based on meeting id
     *
     * @created Anjana H
     *
     * @updated Anjana H 
     *
     * @param object $request - meeting Id
     * 
     * @return success/failure details with view page
     *
     */
    public function meetings_end(Request $request) {

        try {

            DB::begintransaction();

            $meeting_details = Meeting::find($request->meeting_id);
            
            if(!$meeting_details) {

                throw new Exception(tr('meeting_not_found'), 101);
            }

            $meeting_details->status = MEETING_COMPLETED;

            $meeting_details->end_time = date("H:i:s");

            if($meeting_details->save()) {

                DB::commit();

                return redirect()->back()->with('flash_success', tr('meeting_end_success'));
            }
            
            throw new Exception(tr('meeting_end_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());
        }       
         
    }


    /**
     * @method users_subscriptions_index
     *
     * @uses To subscribe a new plans based on users
     *
     * @created Anjana H 
     *
     * @updated Anjana H
     *
     * @param integer $id - User id (Optional)
     * 
     * @return response of array of subscription details
     *
     */
    public function users_subscriptions_index(Request $request) {

        try {

            $user_details = User::find($request->user_id);

            if(!$user_details) {

                throw new Exception(tr('admin_user_not_found'), 101);
            }

            $subscriptions = Subscription::orderBy('created_at','desc')->get();

            $user_subscriptions = UserSubscription::BaseResponse()->where('user_id' , $request->user_id)->orderBy('user_subscriptions.id', 'desc')->paginate(10);

            foreach ($user_subscriptions as $key => $value) {

                $value->plan_text = formatted_plan($value->plan ?? 0);

                $value->expiry_date = common_date($value->expiry_date, Auth::guard('admin')->user()->timezone);
            }

            return view('admin.subscriptions.user_subscriptions')
                        ->with('page', 'users')
                        ->with('sub_page', 'users-view')
                        ->with('user_details', $user_details)
                        ->with('subscriptions', $subscriptions)
                        ->with('user_subscriptions', $user_subscriptions); 
            
        } catch (Exception $e) {

            return back()->with('flash_error', $e->getMessage());
        }       

    }


    /**
     * @method users_subscriptions_save
     *
     * To save subscription details based on user id
     *
     * @created Anjana H
     *
     * @updated Anjana H
     *
     * @param Integer (request) $subscription_id, $user_id
     * 
     * @return success/failure message.
     *
     */
    public function users_subscriptions_save(Request $request) {

        try {
           
            DB::beginTransaction();

            if(!$subscription_details = Subscription::where('id', $request->subscription_id)->Approved()->first()) {

                throw new Exception(tr('admin_subscription_not_found'), 101);
            }

            if(!$users_details = User::find($request->user_id)) {

                throw new Exception(tr('admin_user_not_found'), 101);
            }

            $payment_mode = PAYMENT_BY_ADMIN;

            $request->request->add(['paid_amount' => $subscription_details->amount ?? 0.00, 'payment_id' => "NO-".rand(), 'paid_status' => PAID_STATUS, 'payment_mode' => $payment_mode, 'id' => $request->user_id]);

            $response = \App\Repositories\PaymentRepository::subscriptions_payment_save($request, $subscription_details)->getData();

            if($response->success == true) {
                
                DB::commit();

                return back()->with('flash_success', tr('user_subscription_save'));
            }

            throw new Exception($response->error, $response->error_code);  

        } catch (Exception $e) {

            DB::rollback();

            return back()->with('flash_error', $e->getMessage());
        }

    }

    /**
     * @method users_meetings_clear()
     * 
     * @uses clear ongoing meetings for the user
     *
     * @created Anjana H
     *
     * @updated Anjana H
     *
     * @param
     *
     * @return view page
     */
    public function users_meetings_clear(Request $request) {

        try {

            DB::begintransaction();

            $meetings = Meeting::where('user_id', $request->user_id)->where('status', '!=', MEETING_COMPLETED)->get();
            
            foreach ($meetings as $key => $meeting_details) { 

                \App\MeetingMember::where('meeting_id', $meeting_details->id)->where('status', MEETING_MEMBER_JOINED)->update(['status' => MEETING_MEMBER_ENDED, 'end_time' => date("Y-m-d H:i:s")]);

                $meeting_details->end_time = date("Y-m-d H:i:s");

                $meeting_details->status = MEETING_COMPLETED;

                $meeting_details->save();
                
            }

            DB::commit();
   
            return redirect()->back()->with('flash_success',tr('users_meetings_clear_success'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());
        }   

    }



     /**
     * @method user_subscriptions_view()
     * 
     * @uses used to list the subscription payment
     *
     * @created Ganesh
     *
     * @updated Ganesh
     *
     * @param
     *
     * @return view page
     */
    public function user_subscriptions_view(Request $request) {

        $user_subscription_details = UserSubscription::where('user_subscriptions.id',$request->user_subscription_id)->first();

        $user_subscription_details->user_details = $user_subscription_details->user;

        $user_subscription_details->subscription_details = $user_subscription_details->subscription;


        if(!$user_subscription_details){

            return redirect()->back()->with('flash_error',tr('payment_not_found_error'));

        }

        return view('admin.payments.user_payments_view')
                ->with('user_subscription_details',$user_subscription_details)
                ->with('sub_page','subscription_payments')
                ->withPage('payments');
       
    }

}