<?php

Route::group(['middleware' => 'web'], function() {

    Route::group(['as' => 'admin.', 'prefix' => 'admin'], function(){

        Route::get('important/constants', 'ApplicationController@list_of_constants');

        Route::get('clear-cache', function() {

            $exitCode = Artisan::call('config:cache');

            return back();

        })->name('clear-cache');

        Route::get('login', 'Auth\AdminLoginController@showLoginForm')->name('login');

        Route::post('login', 'Auth\AdminLoginController@login')->name('login.post');

        Route::get('logout', 'Auth\AdminLoginController@logout')->name('logout');

        /***
         *
         * Admin Account releated routes
         *
         */

        Route::get('profile', 'AdminController@profile')->name('profile');

        Route::post('profile/save', 'AdminController@profile_save')->name('profile.save');

        Route::post('change/password', 'AdminController@change_password')->name('change.password');

        Route::get('/', 'AdminController@index')->name('dashboard');
        
        Route::get('users', 'AdminController@users_index')->name('users.index');

        Route::get('users/create', 'AdminController@users_create')->name('users.create');

        Route::get('users/edit', 'AdminController@users_edit')->name('users.edit');

        Route::post('users/save', 'AdminController@users_save')->name('users.save');

        Route::get('users/view', 'AdminController@users_view')->name('users.view');

        Route::get('users/delete', 'AdminController@users_delete')->name('users.delete');

        Route::get('users/status', 'AdminController@users_status')->name('users.status');

        Route::get('users/verify', 'AdminController@users_verify_status')->name('users.verify');

        Route::get('users/meetings_clear', 'AdminController@users_meetings_clear')->name('users.meetings_clear');


        // settings

        Route::get('/admin-control', 'AdminController@admin_control')->name('control');

        Route::get('/ios-control', 'AdminController@ios_control')->name('ios-control'); 

        Route::get('settings', 'AdminController@settings')->name('settings'); 

        Route::post('settings/save', 'AdminController@settings_save')->name('settings.save'); 

        Route::post('env_settings','AdminController@env_settings_save')->name('env-settings.save');

        // STATIC PAGES

        Route::get('static_pages' , 'AdminController@static_pages_index')->name('static_pages.index');

        Route::get('static_pages/create', 'AdminController@static_pages_create')->name('static_pages.create');

        Route::get('static_pages/edit', 'AdminController@static_pages_edit')->name('static_pages.edit');

        Route::post('static_pages/save', 'AdminController@static_pages_save')->name('static_pages.save');

        Route::get('static_pages/delete', 'AdminController@static_pages_delete')->name('static_pages.delete');

        Route::get('static_pages/view', 'AdminController@static_pages_view')->name('static_pages.view');

        Route::get('static_pages/status', 'AdminController@static_pages_status_change')->name('static_pages.status');

        Route::get('/subscriptions', 'AdminController@subscriptions_index')->name('subscriptions.index');

        // Route::get('/subscription/save', 'AdminController@users_subscriptions_save')->name('users.subscriptions.save');

        Route::get('/subscriptions/create', 'AdminController@subscriptions_create')->name('subscriptions.create');

        Route::get('/subscriptions/edit', 'AdminController@subscriptions_edit')->name('subscriptions.edit');

        Route::post('/subscriptions/create', 'AdminController@subscriptions_save')->name('subscriptions.save');

        Route::get('/subscriptions/view', 'AdminController@subscriptions_view')->name('subscriptions.view');

        Route::get('/subscriptions/delete', 'AdminController@subscriptions_delete')->name('subscriptions.delete');

        Route::get('/subscriptions/status', 'AdminController@subscriptions_status_change')->name('subscriptions.status');

        Route::get('/subscriptions/popular/status', 'AdminController@subscriptions_popular_status')->name('subscriptions.popular.status');

        Route::get('/subscriptions/users', 'AdminController@subscriptions_users')->name('subscriptions.users');

        Route::get('subscriptions/payments' , 'AdminController@subscription_payments')->name('subscription.payments');

        Route::get('meetings' , 'AdminController@meetings_index')->name('meetings.index');

        Route::get('meetings/view', 'AdminController@meetings_view')->name('meetings.view');
       
        Route::get('meetings/delete', 'AdminController@meetings_delete')->name('meetings.delete');
        
        Route::get('meetings/end', 'AdminController@meetings_end')->name('meetings.end');

        //User Subscriptions

        Route::get('/users/subscriptions/', 'AdminController@users_subscriptions_index')->name('users.subscriptions');

        Route::get('/users/subscriptions/save', 'AdminController@users_subscriptions_save')->name('users.subscriptions.save');

        Route::get('/users/subscriptions/view' , 'AdminController@user_subscriptions_view')->name('users.subscriptions.view');


    });

});