@extends('layouts.admin')

@section('page_header',tr('subscriptions'))

@section('styles')

<link rel="stylesheet" href="{{asset('admin-assets/css/dropify.min.css')}}">

<link href="{{asset('admin-assets/css/datepicker.css')}}" rel="stylesheet">

@endsection

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.subscriptions.index')}}">{{tr('subscriptions')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)" aria-current="page"></a><span>{{tr('add_subscription')}}</span></li>

@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{tr('add_subscription')}}

            <a class="btn btn-secondary pull-right" href="{{route('admin.subscriptions.index')}}">
                <i class="fa fa-eye"></i> {{tr('view_subscriptions')}}
            </a>
        </h4>

    </div>

    @include('admin.subscriptions._form')

</div>

@endsection