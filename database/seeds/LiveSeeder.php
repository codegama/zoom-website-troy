<?php

use Illuminate\Database\Seeder;

class LiveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('settings')->insert([
    		[
		        'key' => 'OPENVIDU_SERVER_URL',
		        'value' => 'https://streaming.startstreaming.info'
		    ],
		    [
		        'key' => 'OPENVIDU_SECRET',
		        'value' => 'm3server'
		    ],
		]);
        
    }
}
