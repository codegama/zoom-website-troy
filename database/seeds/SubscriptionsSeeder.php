<?php

use Illuminate\Database\Seeder;

class SubscriptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Schema::hasTable('subscriptions')) {

        	DB::table('subscriptions')->insert([
        		[
    		        'title' => 'Monthly Plan',
    		        'description' => 'Monthly Plan',
    		        'no_of_users' => 3,
    		        'subscription_type' => 1,
                    'popular_status' =>1,
                    'plan' => 1,
                    'amount' => 99.99,
                    'status' => 1,
    		        'created_at' => date('Y-m-d H:i:s'),
    		        'updated_at' => date('Y-m-d H:i:s')
    		    ]
            ]);

            DB::table('subscriptions')->insert([
                [
                    'title' => 'Yearly Plan',
                    'description' => 'Yearly Plan',
                    'no_of_users' => 10,
                    'subscription_type' => 1,
                    'popular_status' =>1,
                    'plan' => 1,
                    'amount' => 699.99,
                    'status' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            ]);
        }
    }
}
