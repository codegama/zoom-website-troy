<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class V10AuthRelatedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('users')) {

            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('unique_id');
                $table->string('username');
                $table->string('first_name');
                $table->string('last_name');
                $table->string('name');
                $table->string('email')->unique();
                $table->string('token');
                $table->string('password');
                $table->string('token_expiry');
                $table->string('social_unique_id')->default('');
                $table->string('picture')->default(envfile('APP_URL')."/placeholder.jpg");
                $table->tinyInteger('user_type')->default(0);
                $table->string('dob')->default("");
                $table->text('description')->nullable();
                $table->string('mobile')->default("");
                $table->string('referral_code')->default("");
                $table->text('device_token')->nullable();
                $table->enum('device_type', ['web','android','ios'])->default('android');
                $table->enum('register_type', ['web','android','ios'])->default('android');
                $table->enum('login_by', ['manual','facebook','google','twitter' , 'linkedin'])->default('manual');
                $table->enum('gender', ['male','female','others'])->default('male');
                $table->string('payment_mode')->default(CARD);
                $table->string('user_card_id')->default(0);
                $table->string('timezone')->default('America/Los_Angeles');
                $table->tinyInteger('status')->default(0);
                $table->tinyInteger('registration_steps')->default(0);
                $table->integer('push_notification_status')->default(YES);
                $table->integer('email_notification_status')->default(YES);
                $table->timestamp('email_verified_at')->nullable();
                $table->integer('is_verified')->default(0);
                $table->string('verification_code')->default('');
                $table->string('verification_code_expiry')->default('');
                $table->rememberToken();
                $table->timestamps();
            
            });
        
        }

        if(!Schema::hasTable('admins')) {

            Schema::create('admins', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('email')->unique();
                $table->text('about')->default("");
                $table->string('mobile')->default("");
                $table->string('picture')->default(envfile('APP_URL')."/placeholder.jpg");
                $table->string('password');
                $table->string('timezone')->default('America/Los_Angeles');
                $table->tinyInteger('status')->default(APPROVED);
                $table->rememberToken();
                $table->timestamps();
            });

        }

        if(!Schema::hasTable('user_cards')) {

            Schema::create('user_cards', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->default(uniqid());
                $table->integer('user_id');
                $table->string('card_holder_name')->default("");
                $table->string('card_type');
                $table->string('customer_id');
                $table->string('last_four');
                $table->string('card_token');
                $table->integer('is_default')->default(0);
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });
        
        }

        if(!Schema::hasTable('user_billing_infos')) {

            Schema::create('user_billing_infos', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id');
                $table->string('paypal_email')->nullable();                
                $table->string('account_name')->nullable();
                $table->string('account_no')->nullable();
                $table->string('route_no')->nullable();
                $table->tinyInteger('status')->default(APPROVED);
                $table->timestamps();
            });
        
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');

        Schema::dropIfExists('admins');
    }
}
